//
//  AppDelegate.h
//  utility
//
//  Created by Jumpei Kondo on 2012/10/02.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MainViewController *mainViewController;

@end
