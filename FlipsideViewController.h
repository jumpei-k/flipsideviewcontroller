//
//  FlipsideViewController.h
//  utility
//
//  Created by Jumpei Kondo on 2012/10/02.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController{
    
    IBOutlet UISwitch *mySw;
    NSString* myStr;

}

//プロパティの設定
@property (nonatomic ,retain) NSString *myStr;

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;



//メソッドの宣言
- (IBAction)done:(id)sender;

-(IBAction)changeSw;

@end
