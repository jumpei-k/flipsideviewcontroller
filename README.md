# FlipsideViewController #
Pass value between viewControllers using delegate

## Procedure
``` Objective-C
// FlipsideViewController.h

// Declare protocol
@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

// Declare delegate property
@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;

// FlipsideViewController.m

// Call delegate method
- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];

    
}

//MainViewController.m
// protocol conformance
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    
    dispStr = controller.myStr;
    myLabel.text = dispStr;
    [self dismissViewControllerAnimated:YES completion:nil];
}

```