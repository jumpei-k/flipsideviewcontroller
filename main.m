//
//  main.m
//  utility
//
//  Created by Jumpei Kondo on 2012/10/02.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
