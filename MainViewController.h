//
//  MainViewController.h
//  utility
//
//  Created by Jumpei Kondo on 2012/10/02.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate>{
    IBOutlet UILabel *myLabel;
    NSString *dispStr;
}

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;

- (IBAction)showInfo:(id)sender;

@end
